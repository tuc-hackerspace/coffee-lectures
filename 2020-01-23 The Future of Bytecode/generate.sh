#!/usr/bin/env bash

pandoc --standalone \
	       --mathjax \
	       --to revealjs -o presentation.html \
	       presentation.md

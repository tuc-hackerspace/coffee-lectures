extern crate rand;

mod idiomatic;
use idiomatic::Tree;

fn main() {
    let mut tree = Tree::new();
    let mut cur = 5;
    let mut res = 0;
    for i in 1..10000000 {
        let a = i % 3;
        cur = (cur * 57 + 43) % 10007;
        match a {
            0 => tree.insert(cur),
            1 => tree.erase(cur),
            2 => res += if tree.has_value(cur) { 1 } else { 0 },
            _ => {}
        }
    }
    println!("{}", res);
}

---
title: The Future of Bytecode
subtitle: Abolishing Docker, the JVM & and more
---

## The Past of Bytecode

+ Each language (Java, C#, etc.) has it's own ...
   + ... Bytecode
   + ... Interpreter

## WebAssembly

+ Neither Web (not necessarily)
+ Nor Assembly
+ But still good!

It's not meant to be written ...

## ...But to be generated

Can be generated from:

::: columns
:::: column

+ C
+ C++
+ Go
+ JScript
+ Java

::::
:::: column

+ Kotlin
+ Lua
+ OCAML
+ Python
+ Rust
::::
:::

# But who can run it?

## The Web ...

All major browsers landed WASM support already^[https://caniuse.com/#feat=wasm]:


::: columns
:::: column

+ Edge
+ Firefox
+ Chrome
+ Safari
+ iOS Safari

::::
:::: column

+ Android Browser
+ Opera Mobile
+ Chrome for Android
+ Firefox for Android
+ ...

::::
:::

## ... & Everyone else

+ WebAssembly System Interface (WASI)
   + Standard for running WASM without Browser
+ wasmtime^[https://wasmtime.dev/]
   + WASI implementation
   + can execute WebAssembly
   + runs on all major operating systems

## What else?

+ Secure by default^[https://hacks.mozilla.org/2019/03/standardizing-wasi-a-webassembly-system-interface/]
   + Sandboxing
   + Restrictive by default
   + Capabilities driven Security Concept
+ It's Fast^[using btree implementations from completely-unscientific-benchmarks]™<sup>™</sup>  
   + 2.3 times slower than native version (x86_64 assembler)
   + 2 times slower than Java (openjdk)
   + on par with JScript (node)
+ "WASM already won" ^[https://words.steveklabnik.com/is-webassembly-the-return-of-java-applets-flash#wasm-won_2]
]

## Bonus

::: incremental

+ Cross-
+ Platform-
+ Cross-
+ Language 
+ Libraries ...
+ ... from a single WASM binary!

:::

## References

+ https://webassembly.org
+ https://wasi.dev/
+ https://wasmtime.dev/
+ https://github.com/appcypher/awesome-wasm-langs
